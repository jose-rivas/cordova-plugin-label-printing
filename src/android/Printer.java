package lineten.cordova.plugin;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;

public class Printer extends CordovaPlugin {

    protected static String Actions[] = {"test", "initPrinter"};
    
    protected static boolean Test(String msg, CallbackContext callbackContext) {
    	if (msg == null || msg.length() == 0) {
			callbackContext.error("Empty message!");
		} else {
			callbackContext.success(msg);
		}
    	
    	return true;
    }
    
    protected static boolean initPrinter(CallbackContext callbackContext) {
    	
    }


    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {
    	boolean result = false;

    	switch(action) {
    		case "test": result = Printer.Test(data.getString(0), callbackContext);
    			break;
    		case "initPrinter": result = Printer.initPrinter(callbackContext);
    			break;
    	}
    	
    	return result;
    }
}
