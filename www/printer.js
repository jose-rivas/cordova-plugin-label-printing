/*global cordova, module*/

module.exports = {
    test: function (name, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Printer", "test", [name]);
    }
};
